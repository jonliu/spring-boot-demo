package com.liujun.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

@Controller
public class ThymeleafController {

    @GetMapping("/simple")
    public String simple(HttpServletRequest request){

        ArrayList<String> testArray = new ArrayList<>();
        testArray.add("hello");
        testArray.add("tom");

        request.setAttribute("name", "Tomcat");
        request.setAttribute("testArray", testArray);
        return "test";
    }
}
