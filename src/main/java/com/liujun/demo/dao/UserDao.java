package com.liujun.demo.dao;

import com.liujun.demo.entity.User;

import java.util.List;

public interface UserDao {

    List<User> findAllUsers();

    int insertUser(User user);

    int updUser(User user);

    int delUser(Integer id);
}
