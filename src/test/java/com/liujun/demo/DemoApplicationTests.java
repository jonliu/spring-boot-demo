package com.liujun.demo;

import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Autowired
	private DataSource dataSource;

	@Autowired
	private SqlSession sqlSession;

	@Test
	public void dataSourceTest() throws SQLException {
		/*Connection connection = dataSource.getConnection();
		System.out.println(connection != null);
		connection.close();
		System.out.println("hello ");*/
	}

	@Test
	public void mybatisTest() throws SQLException {
		/*System.out.println(sqlSession != null);
		System.out.println(sqlSession.getClass());*/
	}

	@Test
	public void contextLoads() {
	}

}

